extern crate libusb;

use std::time::Duration;

fn main() {
    let mut context = libusb::Context::new().unwrap();
    // context.set_log_level(libusb::LogLevel::Debug);
    println!("has_hotplug: {}", context.has_hotplug());
    println!("has_hid_access: {}", context.has_hid_access());
    println!("supports_detach_kernel_driver: {}",
             context.supports_detach_kernel_driver());

    for device in context.devices().unwrap().iter() {
        let device_desc = device.device_descriptor().unwrap();

        println!("Bus {:03} Device {:03} ID {:04x}:{:04x}",
                 device.bus_number(),
                 device.address(),
                 device_desc.vendor_id(),
                 device_desc.product_id());

        let device_conf_desc = device.active_config_descriptor().unwrap();

        if device_desc.vendor_id() == 0x046d && device_desc.product_id() == 0xc077 {
            println!("{}", device_desc.num_configurations());
            for interface in device_conf_desc.interfaces() {
                println!("int: {}", interface.number());
                let device_handle = device.open().unwrap();
                let language = device_handle
                    .read_languages(Duration::from_millis(100))
                    .unwrap()
                    [0];
                println!("{:?}", language);
                let string_descriptor = device_handle
                    .read_string_descriptor(language, 1, Duration::from_millis(100))
                    .unwrap();
                println!("String desc: {}", string_descriptor);
                // device_handle.detach_kernel_driver(interface.number()).unwrap();
            }
        }

    }

}
